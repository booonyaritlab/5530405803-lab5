package pengjam.boonyarit.lab5;

public class ThaiPlayer extends Player {
	private String award;

	public ThaiPlayer(String n, int d, int m, int y, double w, double h) {
		super(n, d, m, y, w, h);
	}

	public ThaiPlayer(String n, int d, int m, int y, double w, double h,
			String a) {
		super(n, d, m, y, w, h);
		award = a;
	}

	public String getAward() {
		return award;
	}

	public void setAward(String newaward) {
		award = newaward;
	}

	public void speak() {
		System.out.print("�ٴ������");
		System.out.println();
	}
}