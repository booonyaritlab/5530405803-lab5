package pengjam.boonyarit.lab5;

public class AmericanPlayer extends Player {
	private String facebookAcct;

	public AmericanPlayer(String n, int d, int m, int y, double w, double h) {
		super(n, d, m, y, w, h);
	}

	public AmericanPlayer(String n, int d, int m, int y, double w, double h,
			String f) {
		super(n, d, m, y, w, h);
		facebookAcct = f;
	}

	public String getFacebookAcct() {
		return facebookAcct;
	}

	public void setFacebookAcct(String newfacebookAcct) {
		facebookAcct = newfacebookAcct;
	}
}
