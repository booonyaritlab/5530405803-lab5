package pengjam.boonyarit.lab5;

public class Player extends pengjam.boonyarit.lab4.Player {

	 String language = "English";

	public Player(String n, int d, int m, int y, double w, double h) {
		super(n, d, m, y, w, h);
	}

	public Player(String n, int d, int m, int y, double w, double h, String l) {
		super(n, d, m, y, w, h);
		language = l;
	}

	public void speak() {
		System.out.print("Speak " + language);
		System.out.println();
	}

}
