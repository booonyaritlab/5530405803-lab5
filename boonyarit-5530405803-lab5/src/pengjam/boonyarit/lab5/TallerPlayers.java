package pengjam.boonyarit.lab5;

public class TallerPlayers {
	public static void main(String[] args) {
		ThaiPlayer nusara = new ThaiPlayer("����� ������", 7, 7, 1985, 57, 169,
				"Best Setters");
		AmericanPlayer jeremy = new AmericanPlayer("Jeremy Lin", 2, 12, 1988,
				91, 191, "https://www.facebook.com/jeremylin17");
		ThaiPlayer ratchanok = new ThaiPlayer("�Ѫ�� �Թ�����", 13, 6, 1990,
				50, 160);
		isTaller(ratchanok, jeremy);
		isTaller(jeremy, nusara);
	}

	private static void isTaller(Player p1, Player p2) {
		if (p1.getHeight() > p2.getHeight())
			System.out
					.println(p1.getName() + " is taller than " + p2.getName());
		else
			System.out.println(p1.getName() + " is not taller than "
					+ p2.getName());
	}
}